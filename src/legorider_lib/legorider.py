import rospy
import math
import time
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from rosrider_description.srv import CameraSensor, LaserSensor
from std_srvs.srv import Trigger
import os
import requests

class Robot:
    def __init__(self, robot_name):
        rospy.init_node("legorider_controller")

        self.robot_cmd_vel = rospy.Publisher("/legorider/cmd_vel", Twist, queue_size=10)

        self.image = self._Image()

        self._init_sensor_services()
        self._init_laser_services()

        self.initial_speed = 0
        self.ach4_test_flag = False

        rospy.sleep(0.5)

        self.last_linear_speed = 0
        self.last_angular_speed = 0

        self._init_game_controller()


    def _init_sensor_services (self):
        try:
            rospy.wait_for_service("/front_camera/legorider_get_image", 0.5)

            self.image_data_service = rospy.ServiceProxy('/front_camera/legorider_get_image', CameraSensor)

            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            return True

        except (rospy.ServiceException, rospy.ROSException):
            print("no camera sensor")
            self.status = "stop"
            return False

    def _init_laser_services (self):
        try:
            rospy.wait_for_service("/legorider/get_front_range", 0.5)

            self.laser_service = rospy.ServiceProxy('/legorider/get_front_range', LaserSensor)

            resp = self.laser_service()

            return True

        except (rospy.ServiceException, rospy.ROSException):
            print("no laser sensor")
            self.status = "stop"
            return False

    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)

            resp = self.robot_handle()
            self.status = resp.message

        except (rospy.ServiceException, rospy.ROSException):
            print("free_roam")
            self.status = "free_roam"

    def _check_game_controller(self):
        resp = self.robot_handle()
        self.status = resp.message

    def move(self, linear_speed):
        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "stop":
                return

            twist_msg = Twist()
            twist_msg.linear.x = linear_speed
            twist_msg.angular.z = self.last_angular_speed
            self.last_linear_speed = linear_speed

            self.robot_cmd_vel.publish(twist_msg)

    def rotate(self, angular_speed):

        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "stop":
                return

            twist_msg = Twist()
            twist_msg.angular.z = angular_speed
            twist_msg.linear.x = self.last_linear_speed
            self.last_angular_speed = angular_speed

            self.robot_cmd_vel.publish(twist_msg)

    def get_sensor_data(self):
        image = self.image_data()
        sensor_row = []
        for i in range(image.width):
            brightness = (0.2126 * ord(image.data[i * 3])) + (0.7152 * ord(image.data[i * 3 + 1])) + (0.0722 * ord(image.data[i * 3 + 2]))
            sensor_row.append(brightness)
        return sensor_row

    # MDM - just adding this quick as copy from drone - it works - not sure why other form doesn't
    # Will clean up later after deadline resolved
    def get_camera_data(self):
        resp = self.image_data_service()

        image_rgb_data = []

        for i in range(self.image.width * self.image.height * 3):
            image_rgb_data.append(ord(resp.data[i]))

        return image_rgb_data

    def image_data(self):
        resp = self.image_data_service()

        self.image.data = resp.data

        return self.image


    def laser_data(self):
        resp = self.laser_service()

        return resp.data


    def is_ok(self):
        if not rospy.is_shutdown():
            self.image_data()
            return True
        else:
            return False

    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []
