cmake_minimum_required(VERSION 3.0.2)
project(legorider_lib)

find_package(catkin REQUIRED COMPONENTS
  rospy
  std_msgs
  geometry_msgs
  message_generation
)

catkin_python_setup()

#add_service_files(
#  FILES
#)

#generate_messages(
#  DEPENDENCIES std_msgs geometry_msgs
#)

catkin_package(
  CATKIN_DEPENDS
    rospy
    message_runtime
    std_msgs
)

catkin_package()

include_directories(
  ${catkin_INCLUDE_DIRS}
)
