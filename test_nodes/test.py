#!/usr/bin/env python
from legorider_lib.legorider import Robot
import rospy
import math

robot = Robot("legorider")

#Your code starts here:
#-1 right 1 left
last_rotation = -1

while(robot.is_ok()):
    image_height = robot.image.width
    image_width = robot.image.width
    image_data = robot.image_data()

    robot.move(1)
    robot.rotate(0.1)

    print(robot.laser_data())
